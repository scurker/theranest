'use strict';

var React = require('react')
  , { object, bool } = React.PropTypes
  , classnames = require('classnames')
  , api = require('../lib/api');

var Appointment = React.createClass({

  propTypes: {
    appointment: object.isRequired,
    isToday: bool.isRequired
  },

  getDefaultProps() {
    return {
      appointment: {},
      isToday: false
    };
  },

  render() {
    var { appointment, isToday } = this.props;
    return (
      <div className="row">
        <div className="col-sm-3">
          <span className="secondary-text">{!isToday && appointment.startDate} {appointment.startTime} - {appointment.endTime}</span>
        </div>
        <div className="col-sm-9">
          {appointment.clientName}
        </div>
      </div>
    );
  }

});

var Appointments = React.createClass({

  getInitialState() {
    return {
      appointments: [],
      mode: 'Today'
    };
  },

  componentDidMount() {
    this.fetchAppointments();
  },

  componentDidUpdate(prevProps, prevState) {
    if(this.state.mode !== prevState.mode) {
      this.fetchAppointments();
    }
  },

  setMode(mode) {
    this.setState({ mode: mode });
  },

  fetchAppointments() {
    api(`appointments?mode=${this.state.mode}`)
      .then(response => {
        this.setState({
          appointments: response
        })
      });
  },

  render() {
    var { mode } = this.state
      , isToday = mode === 'Today';

    return (
      <div className="panel panel-default">
        <div className="panel-heading">
          <h3 className="panel-title" style={{display: 'inline-block', marginRight: '1em'}}>Appointments</h3>
          <div className="btn-group" role="group">
            <button type="button" className={classnames('btn btn-default', { active: mode === 'Past' })} onClick={() => this.setMode('Past')}>Past</button>
            <button type="button" className={classnames('btn btn-default', { active: mode === 'Today' })} onClick={() => this.setMode('Today')}>Today</button>
            <button type="button" className={classnames('btn btn-default', { active: mode === 'Future' })} onClick={() => this.setMode('Upcoming')}>Upcoming</button>
          </div>
        </div>
        <ul className="list-group">
          {this.state.appointments.map(appointment => {
            return (
              <li className="list-group-item" key={appointment.id}>
                <Appointment appointment={appointment} isToday={isToday} />
              </li>
            );
          })}
        </ul>
      </div>
    );
  }

});

module.exports = Appointments;