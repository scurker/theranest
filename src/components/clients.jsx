'use strict';

var React = require('react')
  , LinkedStateMixin = require('react-addons-linked-state-mixin')
  , { object } = React.PropTypes
  , Nav = require('./nav')
  , api = require('../lib/api');

var Clients = React.createClass({

  mixins: [LinkedStateMixin],

  getInitialState() {
    return {
      locations: [],
      showSuccess: false,
      firstName: '',
      middleName: '',
      lastName: '',
      locationId: ''
    }
  },

  handleSubmit(e) {
    e.preventDefault();
    return api('clients', {
      method: 'post',
      headers: {
        'content-type': 'application/json'
      },
      body: JSON.stringify(this.state)
    })
    .then(() => {
      var state = this.getInitialState();
      state.locations = this.state.locations;
      state.showSuccess = true;
      this.setState(state);
    });
  },

  fetchLocations() {
    return api('locations');
  },

  componentDidMount() {
    this.fetchLocations().then(locations => {
      this.setState({ locations: locations.locations });
    });
  },

  render() {
    var { locations, showSuccess } = this.state
      , successMessage;

    if(showSuccess) {
      successMessage = <div className="alert alert-success"><strong>Good job!</strong> You successfully added a new client.</div>;
    }

    return (
      <div className="panel panel-default" onSubmit={this.handleSubmit}>
        <div className="panel-heading">
          <h3 className="panel-title">Add Client</h3>
        </div>
        <div className="panel-body">
          {successMessage}
          <form method="post" action="/client">
            <div className="row">
              <div className="form-group col-sm-4">
                <label htmlFor="firstName">First Name</label>
                <input type="text" className="form-control" id="firstName" valueLink={this.linkState('firstName')} />
              </div>
              <div className="form-group col-sm-4">
                <label htmlFor="middleName">Middle Name</label>
                <input type="text" className="form-control" id="middleName" valueLink={this.linkState('middleName')} />
              </div>
              <div className="form-group col-sm-4">
                <label htmlFor="lastName">Last Name</label>
                <input type="text" className="form-control" id="lastname" valueLink={this.linkState('lastName')} required />
              </div>
            </div>
            <div className="form-group">
              <label htmlFor="location">Location</label>
              <select id="location" className="form-control" valueLink={this.linkState('locationId')} required>
                <option>Select a Location</option>
                {locations.map(x => <option key={x.id} value={x.id}>{x.name}</option>)}
              </select>
            </div>
            <button type="submit" className="btn btn-default">Add Client</button>
          </form>
        </div>
      </div>
    );
  }

});

module.exports = Clients;