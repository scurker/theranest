'use strict';

var React = require('react')
  , { object } = React.PropTypes
  , { browserHistory } = require('react-router')
  , LinkedStateMixin = require('react-addons-linked-state-mixin');

var Login = React.createClass({

  mixins: [LinkedStateMixin],

  contextTypes: {
    router: object
  },

  getInitialState() {
    return {
      email: '',
      password: '',
      isInvalid: false
    }
  },

  handleSubmit(e) {
    e.preventDefault();

    var { email, password } = this.state;

    fetch('https://api.theranest.com/account/sign-in', {
        method: 'post',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email, password })
      })
      .then((response) => {
        if(response.status === 200) {
          return response;
        } else {
          throw error;
        }
      })
      .then((response) => response.json())
      .then((response) => {
        sessionStorage.token = response;
        browserHistory.push('/appointments');
      })
      .catch((err) => {
        this.setState({ isInvalid: true });
      });
  },

  render() {
    var invalidMessage;

    if(this.state.isInvalid) {
      invalidMessage = (<div className="alert alert-danger">Invalid username and/or password.</div>);
    }

    return (
      <div className="row">
        <div className="col-md-4 col-md-offset-4">
          <h1>Login</h1>
          <div className="panel panel-default">
            <div className="panel-body">
              {invalidMessage}
              <form onSubmit={this.handleSubmit}>
                <div className="form-group">
                  <label htmlFor="email">Email</label>
                  <input id="email" className="form-control" type="text" name="email" valueLink={this.linkState('email')} />
                </div>
                <div className="form-group">
                  <label htmlFor="password">Password</label>
                  <input type="password" className="form-control" name="password" valueLink={this.linkState('password')} />
                </div>
                <button type="submit" className="btn btn-default">Login</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }

});

module.exports = Login;