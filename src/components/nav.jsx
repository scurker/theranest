'use strict';

var React = require('react')
  , { Link } = require('react-router');

var Nav = React.createClass({

  render() {
    return (
      <nav className="navbar navbar-default">
        <ul className="nav navbar-nav">
          <li><Link to="/appointments">Appointments</Link></li>
          <li><Link to="/clients">Clients</Link></li>
        </ul>
      </nav>
    );
  }

});

module.exports = Nav;