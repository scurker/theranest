'use strict';

var React = require('react')
  , Nav = require('./nav');

var Layout = React.createClass({

  getInitialState() {
    return {
      loggedIn: typeof sessionStorage.token !== 'undefined'
    };
  },

  render() {
    var nav;

    if(this.state.loggedIn) {
      nav = <Nav />;
    }

    return (
      <div>
        <Nav />
        {this.props.children}
      </div>
    );
  }

});

module.exports = Layout;