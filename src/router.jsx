'use strict';

var React = require('react')
  , { render } = require('react-dom')
  , { Router, browserHistory } = require('react-router')
  , routes = require('./routes');

if (typeof window !== 'undefined') {
  render(
    <Router history={browserHistory}>{routes}</Router>,
    document.querySelector('.container.app')
  );
}

module.exports = <Router>{routes}</Router>;