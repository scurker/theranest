'use strict';

var assign = require('deep-assign');

module.exports = function(url, options = {}) {
  return fetch(`https://api.theranest.com/${url}`, assign(options, {
    headers: {
      'Authorization': ['Bearer', sessionStorage.getItem('token')].join(' ')
    }
  })).then(response => response.json());
};