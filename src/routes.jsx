'use strict';

var React = require('react')
  , { IndexRoute, Route, browserHistory } = require('react-router')
  , Layout = require('./components/layout')
  , Login = require('./components/login')
  , Appointments = require('./components/appointments')
  , Clients = require('./components/clients');

module.exports = (
  <Route path="/" component={Layout}>
    <IndexRoute component={Login} />
    <Route path="appointments" component={Appointments} />
    <Route path="clients" component={Clients} />
  </Route>
);