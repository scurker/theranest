'use strict';

var React = require('react')
  , ReactDOM = require('react-dom')
  , router = require('./router')
  , fetch = require('whatwg-fetch');