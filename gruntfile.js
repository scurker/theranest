'use strict';

var webpack = require('webpack');

module.exports = function (grunt) {

  require('matchdep').filterAll('grunt-*').forEach(grunt.loadNpmTasks);

  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),

    config: {
      assets: 'assets',
      js: '<%= config.assets %>/js',
      scss: '<%= config.assets %>/scss',
      node_modules: 'node_modules',
      dest: 'public',
      base_dir: 'src',
    },

    jshint: {
      options: {
        jshintrc: true
      },
      files: [
        'gruntfile.js',
        '<%= config.js %>/{,*/}*.js',
        '<%= config.base_dir %>/**/*.js',
        '!<%= config.dest %>/**/*.js'
      ]
    },

    sass: {
      options: {
        sourceMap: true,
        includePaths: ['<%= config.node_modules %>']
      },
      dist: {
        files: {
          '<%= config.dest %>/css/global.css': '<%= config.scss %>/global.scss'
        }
      }
    },

    postcss: {
      options: {
        map: {
          inline: false, // save all sourcemaps as separate files...
        },
        processors: [
          require('autoprefixer')({ browsers: ['last 3 versions', 'ie >= 9'] })
        ]
      },
      dist: {
        files: [{
          expand: true,
          cwd: '<%= config.dest %>/css',
          src: '{,*/}*.css',
          dest: '<%= config.dest %>/css'
        }]
      }
    },

    webpack: {
      options: {
        module: {
          loaders: [{
            test: /\.jsx?$/,
            exclude: /node_modules/,
            loader: 'babel-loader'
          }]
        },
        resolve: {
          extensions: ['', '.jsx', '.js']
        },
        devtool: 'source-map',
        plugins: [
          new webpack.optimize.DedupePlugin(),
          new webpack.optimize.UglifyJsPlugin({ output: { comments: false }}),
        ]
      },
      dist: {
        entry: ['./src/index'],
        output: {
          path: '<%= config.dest %>/js',
          filename: 'global.js'
        },
      }
    }

  });

  grunt.registerTask('css', [
    'sass',
    'postcss',
  ]);

  grunt.registerTask('js', [
    'webpack',
    // 'jshint'
  ]);

  grunt.registerTask('default', [
    'css',
    'js'
  ]);

};